#!/usr/bin/env python
#
# This will update the piece and prototype definitions in a log file
# based on an updated log file.
#
# We have a log file, say `Tutorial.vlog' which has the old
# definitions of pieces and prototypes.  We open that log file in
# VASSAL
#
#    Tools->Load log, fast-forward, and append 
#
# giving the log file a new name, say `Tutorial2.vlog`, and execute
#
#    Tools->Update pieces
#
# This will write the new piece and prototype definitions at the _end_
# of `Tutorial2.vlog`, which causes some hick-ups when loading the log
# file.
#
# What we do here, is we load in `Tutorial2.vlog`, and find the
# mapping between old piece and prototype definitions to the new piece
# and prototype definitions (get_mapping).
#
# We then open the old log file `Tutorial.vlog`, and replace the
# initial piece and prototype definitions in that with those found at
# the end of the new log file `Tutorial2.vlog`
#
# Note, pieces are fully expanded in the log files.  That means that
# there are no prototype definitions in the log file.  Instead, the
# piece prototypes are written into the piece definition.

from wgvsav import *

def get_piece(line):
    from re import match

    m = match(r'LOG\s+\+/([0-9]+)/.*;([a-z0-9_]+)\.png.*',line)
    if m is None:
        return None,None

    iden = int(m.group(1))
    icon = m.group(2)
    return iden, icon
    
    
def get_mapping(filename='Tutorial2.vlog'):
    # Get key an content 
    key, lines = read_save(filename)

    tmp = {}
    # Loop over the lines and extract piece and prototype definitions
    for line in lines:
        # print(line)
        iden,icon = get_piece(line)
        if iden is None:
            continue

        if icon not in tmp:
            tmp[icon] = []

        tmp[icon].append((iden,line))

    mapping = {}
    for k,v in tmp.items():
        if len(v) != 2:
            print(f'Piece {k} only has one definition')
            continue

        old_iden = v[0][0]
        new_iden = v[1][0]
        old_line = v[0][1]
        new_line = v[1][1]
        new_line = new_line.replace(f'+/{new_iden}/',f'+/{old_iden}/')
        
        mapping[old_line] = new_line

        # if old_line != new_line:
        #     print(f'{k:20s} updated')
        # else:
        #     print(f'{k:20s} the same')
        
    return mapping

def rewrite(filename,mapping):
    key, lines = read_save(filename)


    new_lines = []
    for line in lines:
        iden,icon = get_piece(line)
        if iden is None:
            new_lines.append(line)
            continue

        new_line = mapping.get(line,None)
        if new_line is None:
            print(f'Line "{line}" not updated')
            new_lines.append(line)
            continue
        if new_line == line:
            print(f'{icon} the same')
        else:
            print(f'{icon} updated')
            

        # print(f'Updating {icon}')
        new_lines.append(new_line)


    write_save(filename+'.new',key,new_lines)

    
        
        
        
# get_mapping()


    
    
