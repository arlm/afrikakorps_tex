from wgexport import *

moreHelp = '''
<html>
 <head>
  <title>{title}</title>
  <style>
  </style>
 </head>
 <body>
  <h1>{title}</h1>
  <h2>Tips</h2>
  <ul>
   <li>
    Use the <b>Turn tracker</b> in the menu bar (short cuts
    <code>Alt-T</code> to move forward, <code>Alt-Shift-T</code> to
    move backward), to keep track of the turn and phases.  The module
    will take appropriate actions. 
    <ul>
     <li>Before the <b>Allied supply phase</b>, mark all isolated 
      Allied units as twice isolated</li>    
     <li>After the <b>Allited victory</b> phase, eliminate all
      Allied units that are twice isolated.</li>
     <li>Before the <b>Axis supply phase</b>, mark all isolated
      Axis units as twice isolated</li>    
     <li>After the <b>Axis combat phase</b> phase, eliminate all
      Axis units that are twice isolated.</li>
    </ul>
    Note that game turn marker cannot be moved by hand.  Only the turn
    track interface will allow movement of the game turn marker.
   </li>
   <li>Eliminated units are sent to the respective &quot;dead pool&quot;,
     <i>except</i> supply units which are returned to the OOBs</li>
   <li>The replacement point (RP) tracks are in the &quot;dead pool&quot;
     of each faction.  Remember to adjust these after turn 23 (March '42)
     when each faction start to accumulate RPs and when ever replacement
     points are used.

     There is one marker to replace tens of RPs (e.g., 0, 10, 20), and
     another for ones of RPs (e.g., 0, 1, 2, ..., 9).  For example, to
     indicate a faction has 23 RPs, place the &quot;10&quot; marker in
     the &quot;2&quot; slot and the &quot;1&quot; marker in the
     &quot;3&quot;
   </li>
   <li><img class="icon" src="ak-icon.png"/> Press the
    <i>Afrika Korps</i> button (<code>Ctrl-Shift-G</code>) to load the initial
     Axis setup.</li>
   <li><img class="icon" src="8th-icon.png"/> Press the <i>8th army</i>
    button (<code>Ctrl-Shift-A</code>) to load the Allied setup.</li>
   <li>Replacements and reinforcements can easily be placed in their
     arrival hex by right-clicking and selecting &quot;To
     Tobruk&quot; or &quot;To base&quot;
   <li><img src="isolated-icon.png"> The menu button <b>Show unit
    status</b> (<code>Ctrl-S</code>) shows all units divided by supply
    status (with isolated marker). Use this to quickly find
    units that may need checking. Note that the module cannot by
    itself figure out the supply status of a unit.  This must be
    checked by a player.  However, once you have flipped a unit
    because it is out of supply, then the above turn actions will take
    care of most of it for you.  Note, the factions should flip
    units back face-up in the victory phases if a unit has
    come back into supply.  Otherwise, it will automatically be
    eliminated.  </li>
  <li>Initially, isolated markers must be added manually.  This can
    only happen in the supply phases.</li>
  </ul>
  <h2>About this game</h2>
  <p>
   This VASSAL module was created from
   L<sup>A</sup>T<sub>E</sub>X sources of a Print'n'Play version
   of the <i>D-Day</i> game.  That (a PDF) can be
   found at
  </p>
  <center>
   <code>https://gitlab.com/wargames_tex/afrikakorps_tex</code></a>
  </center>
  <p>
   where this module, and the sources, can also be found.  The PDF
   can also be found as the rules of this game, available in the
   <b>Help</b> menu.
  </p>
  <p>
   The original game was release by the Avalon Hill Game Company.
  <h3>Credits</h3>
  <dl>
   <dt>Original Research and Design:</dt><dd>Charles Roberts</dd>
   <dt>Original Development:</dt><dd>Thomas N. Shaw</dd>
   <dt>Third Edition Rewrite:</dt><dd>Donald Greenwood</dd>
   <dt>Rulebook Cover Art:</td><dd>Rodger MacGowan</dd>
   <dt>Playtesting:</dt>
   <dd>Frank Preissle, Bruno Sinigaglio, Dale Garbutt, Alan R. Moon,
    Donald Burdick</dd>
   <dt>Typesetting:</dt><dd>Colonial Composition</dd>
   <dt>Printing:</dt><dd>Monarch Services, Inc.</dd>
  </dl>
  <h2>Copyright and license</h2>
  <p>
   This work is &#127279; 2022 Christian Holm Christensen, and
   licensed under the Creative Commons Attribution-ShareAlike 4.0
   International License. To view a copy of this license, visit
  </p>
  <center>
   <code>http://creativecommons.org/licenses/by-sa/4.0</code>
  </center>
  <p>
   or send a letter to
  </p>
  <center>
   Creative Commons<br>
   PO Box 1866<br>
   Mountain View<br>
   CA 94042<br>
   USA
  </center>
</body>
</html>'''

# --------------------------------------------------------------------
# Calculate Hex coordinates from string 
def convHex(loc):
    try:
        int(loc)
        return None
    except:
        pass
    if '+' in loc:
        return None

    from math import floor 
    ordz       = ord('Z')
    orda       = ord('A')
    dz         = ordz - orda + 1
    acol, arow = loc[0], loc[1:]
    row        = int(arow)
    col        = ord(acol[0])-orda
    off        = int(col//2)
    row        = row - off
    icol       = col % dz
    tcol       = chr(icol + orda)
    return f'{tcol}{row}'

# --------------------------------------------------------------------
# Our main patching function
def patch(build,data,vmod,verbose=False):
    from re  import sub
    from PIL import Image
    from io  import BytesIO
    
    vmod.addExternalFile('ak/ObliqueHexGridNumbering.class')
    game = build.getGame()

    notSetup = 'NotInitialSetup'
    game.getGlobalProperties()[0]\
        .addProperty(name         = notSetup,
                     initialValue = False,
                     description  = 'Whether we are not at the start')
    
    # ----------------------------------------------------------------
    # Extra documentation 
    doc  = game.getDocumentation()[0]
    doc.addHelpFile(title='More information',fileName='help/more.html')
    vmod.addFile('help/more.html', moreHelp.format(title=game['name']))
    
    maps = game.getMaps()
    main = maps['Board']

    # ----------------------------------------------------------------
    # Get the restore global key, set icon and remove the deadmap map,
    # since we will move eliminated units to the OOBs instead.
    # restore = maps['DeadMap'].getMassKeys()['Restore']
    # restore['icon'] = 'restore-icon.png'
    # game.remove(maps['DeadMap'])
    # main.append(restore)
    pool = maps['DeadMap']
    pool['icon'] = 'pool-icon.png'
    pool.getMassKeys()['Restore']['icon'] = 'restore-icon.png'
    pool.addMassKey(name         = 'Decrement',
                    buttonHotkey = key('-',ALT),
                    hotkey       = key('-',ALT),
                    buttonText   = '', # g['name']
                    icon         = '/icons/32x32/go-previous.png',
                    reportSingle = True,
                    reportFormat = '',
                    tooltip      = 'Decrement RP counter(s)')
    pool.addMassKey(name         = 'Increment',
                    buttonHotkey = key('=',ALT_SHIFT),
                    hotkey       = key('=',ALT_SHIFT),
                    buttonText   = '', # g['name']
                    icon         = '/icons/32x32/go-next.png',
                    reportSingle = True,
                    reportFormat = '',
                    tooltip      = 'Increment RP counter(s)')
    
    oobs            = game.getChartWindows()['OOBs']
    oobs['icon']    = 'oob-icon.png'

    # ----------------------------------------------------------------
    # Fix-up RP markers
    atstarts = pool.getAtStarts(single=False)
    print('Pool at start:',atstarts)
    addto = {}
    for atstart in atstarts:
        if atstart['name'].endswith('1'):
            atstart['name']     = atstart['name'].replace('1','0')
            atstart['location'] = atstart['location'].replace('1','0')
            addto[atstart['name']] = atstart
        else:
            pool.remove(atstart)
    addto['uk rp 0'].addPiece(game.getPieces(asdict=True)['uk rp 10'])
    addto['de rp 0'].addPiece(game.getPieces(asdict=True)['de rp 10'])

    # ----------------------------------------------------------------
    # Set custom icons on some global keys 
    mkeys = main.getMassKeys()
    mkeys['Eliminate']['icon'] = 'eliminate-icon.png'

    # ----------------------------------------------------------------
    # Add global key to set isolated status 
    main.addMassKey(name         = 'Add isolated marker',
                    buttonHotkey = key('I'),
                    hotkey       = key('I'),
                    buttonText   = '', # g['name']
                    icon         = 'isolated-icon.png',
                    reportSingle = True,
                    reportFormat = '',#Adding isolated marker to selected',
                    tooltip      = 'Add isolated marker')
    main.addMassKey(name         = 'Remove isolated marker',
                    buttonHotkey = key('F'),
                    hotkey       = key('F'),
                    buttonText   = '', # g['name']
                    icon         = 'supplied-icon.png',
                    reportSingle = True,
                    reportFormat = '',#Remove isolated marker from selected',
                    tooltip      = 'Remove isolated marker from seleced')

    # ----------------------------------------------------------------
    # Get Zoned area
    zoned = main.getBoardPicker()[0].getBoards()['Board'].getZonedGrids()[0]
    # ----------------------------------------------------------------
    # Adjust the hexes
    #
    # Get the hex grid and turn of drawing
    zones  = zoned.getZones()
    hzone  = zones['hexes']
    hgrids = hzone.getHexGrids()
    hgrid  = hgrids[0]
    hnum   = hgrid.getNumbering()[0]
    hgrid.remove(hnum)
    # Copy the old parameters
    hnattr = {k:v for k,v in hnum.getAttributes().items()}
    # Set new parameters 
    hnattr['hOff']      = 0
    hnattr['vOff']      = -9
    hgrid.addNode('ak.ObliqueHexGridNumbering',**hnattr)
    zoned.remove(hzone)
    zoned.append(hzone)


    # ----------------------------------------------------------------
    turns         = game.getTurnTracks()['Turn']
    phaseNames    = ['Axis supply',
                     'Axis reinforcements',
                     'Axis movement',
                     'Axis combat',
                     'Axis victory',
                     'Allied supply',
                     'Allied reinforcements',
                     'Allied movement',
                     'Allied combat',
                     'Allied victory']
    phases         = turns.getLists()['Phase']
    phases['list'] = ','.join(phaseNames)
    axisSupply     = phaseNames[0]
    axisCombat     = phaseNames[3]
    axisElim       = phaseNames[4]
    alliedSupply   = phaseNames[5]
    alliedCombat   = phaseNames[-2]
    alliedElim     = phaseNames[-1]
    # ----------------------------------------------------------------
    # Define global keys for turn marker
    # 
    # - A key when moving from allied to german
    # - A key when moving from german to allied
    # - A key when entering the german turn so we can eliminate
    #   isolated Allied units
    # - A key when entering the Allied turn so we can mark out-of-supply
    #   Allied units with the isolated marker
    #
    # Below we define the corresponding global keys
    elimTwice  = key(NONE,0)+',elimTwice' # key('E',CTRL_SHIFT)
    markTwice  = key(NONE,0)+',markTwice' # key('I',CTRL_SHIFT)
    testInit   = key(NONE,0)+',testInitial' # key
    isInit     = key(NONE,0)+',isInitial'
    elimKey    = key(NONE,0)+',eliminate'
    turns.addHotkey(hotkey       = elimTwice,
                    match        = f'{{Phase=="{alliedElim}"}}',
                    #reportFormat = '- Eliminating twice isolated Allied units',
                    name         = 'Allied attrition')
    turns.addHotkey(hotkey       = elimTwice,
                    match        = f'{{Phase=="{axisElim}"}}',
                    #reportFormat = '- Eliminating twice isolated Axis units',
                    name         = 'Axis attrition')
    turns.addHotkey(hotkey       = markTwice,
                    match        = f'{{Phase=="{alliedSupply}"}}',
                    #reportFormat = '--- Mark twice isolated Allied units ---',
                    name         = 'Allied supply')
    turns.addHotkey(hotkey       = markTwice,
                    match        = f'{{Phase=="{axisSupply}"}}',
                    #reportFormat = '--- Mark twice isolated Axis units ---',
                    name         = 'Axis isolation')
    turns.addHotkey(hotkey       = testInit,
                    match        = '{true}',
                    reportFormat = ''
                    #reportFormat = '{"Test initial (turn) "'+\
                    #               '+Turn+"/"+Phase+": "+notSetup}'
                    )
    # Add some global keys to the map
    #
    # - Flip turn marker (Ctrl+Shift+F)
    # - Mark twice out-of-supply Allied units (Ctrl+Shift+I)
    # - Eliminate twice isolated Allied units (Ctrl+Shift+E)
    main.addMassKey(name         = 'Mark twice isolated Allied units',
                    buttonHotkey = markTwice,
                    hotkey       = key('I',CTRL),
                    buttonText   = '', 
                    target       = '',
                    #reportFormat = 'Mark twice isolated Allied units',
                    filter       = f'{{Phase=="{alliedSupply}"&&'\
                                      'Faction=="Allied"&&Isolated_Level==2}')
    #
    main.addMassKey(name         = 'Mark twice isolated Axis units',
                    buttonHotkey = markTwice,
                    hotkey       = key('I',CTRL),
                    buttonText   = '', 
                    target       = '',
                    #reportFormat = 'Mark twice isolated Axis units',
                    filter       = f'{{Phase=="{axisSupply}"&&'\
                                      'Faction=="Axis"&&Isolated_Level==2}')
    #
    main.addMassKey(name         = 'Eliminate twice isolated Allied units',
                    buttonHotkey = elimTwice,
                    hotkey       = key('E',CTRL),
                    buttonText   = '', 
                    target       = '',
                    #reportFormat = 'Eliminate twice isolated Allied units',
                    filter       = f'{{Phase=="{alliedElim}"&&'\
                                      'Faction=="Allied"&&Isolated_Level==3}')
    #
    main.addMassKey(name         = 'Eliminate twice isolated Axis units',
                    buttonHotkey = elimTwice,
                    hotkey       = key('E',CTRL),
                    buttonText   = '', 
                    target       = '',
                    #reportFormat = 'Eliminate twice isolated Axis units',
                    filter       = f'{{Phase=="{axisElim}"&&'\
                                      'Faction=="Axis"&&Isolated_Level==3}')
    # 
    main.addMassKey(name         = 'Axis setup',
                    icon         = 'ak-icon.png',
                    buttonHotkey = key('G',CTRL_SHIFT),
                    hotkey       = key(NONE,0)+f',startHex',
                    buttonText   = '',
                    target       = '',
                    singleMap    = False,
                    canDisable   = True,
                    propertyGate = notSetup,
                    reportFormat = '!Load Axis setup',
                    tooltip      = 'Load Axis setup',
                    filter       = '{Faction=="Axis"}')
    main.addMassKey(name         = 'Allied setup',
                    icon         = '8th-icon.png',
                    buttonHotkey = key('A',CTRL_SHIFT),
                    hotkey       = key(NONE,0)+f',startHex',
                    buttonText   = '',
                    target       = '',
                    singleMap    = False,
                    canDisable   = True,
                    propertyGate = notSetup,
                    reportFormat = '!Load Allied setup',
                    tooltip      = 'Load Allied setup',
                    filter       = '{Faction=="Allied"}')
    main.addMassKey(name         = 'Test for initial setup',
                    icon         = '',
                    buttonHotkey = testInit,
                    hotkey       = isInit,
                    buttonText   = '',
                    target       = '',
                    tooltip      = 'Test for initial setup',
                    reportFormat = '',
                    #reportFormat = '{"Test initial (mass) "+' + \
                    #               'Turn+"/"+Phase+": "+notSetup}',
                    filter       = '{BasicName == "game turn"}')


    # ----------------------------------------------------------------
    # Loop over turns, and create prototype for game turn marker to
    # move it along the turn track.  We do that by adding commands to
    # the turn track, which then delegates to the prototype.
    #
    # We add the prototypeContainer to the game turn marker later on. 
    prototypeContainer = game.getPrototypes()[0]
    prototypes         = prototypeContainer.getPrototypes()
    turnPrototypes     = []
    for t in range(1,39):
        k = key(NONE,0)+f',Turn{t}'
        turns.addHotkey(hotkey       = k,
                        match        = (f'{{Turn=={t}&&'
                                        f'Phase=="{phaseNames[0]}"}}'),
                        reportFormat = f'=== Turn {t} ===',
                        name         = f'Turn {t}')
        main.addMassKey(name         = f'Turn marker to {t}',
                        buttonHotkey = k,
                        hotkey       = k,
                        buttonText   = '', 
                        target       = '',
                        filter       = ('{BasicName == "game turn"}'))
        pn     = f'To turn {t}'
        traits = [ SendtoTrait(mapName     = 'Board',
                               boardName   = 'Board',
                               name        = '',
                               restoreName = '',
                               restoreKey  = '',
                               zone        = f'Turn track',
                               destination = 'R', #R for region
                               region      = ('game turn' if t == 1 else
                                              f'turn {t}'),
                               key         = k,
                               x           = 0,
                               y           = 0),
                   BasicTrait()]
        prototypeContainer.addPrototype(name        = f'{pn} prototype',
                                        description = f'{pn} prototype',
                                        traits      = traits)
        turnPrototypes.append(pn)

    # ----------------------------------------------------------------
    # Increment, decrement RPs
    for fac, faction in [['uk', 'Allied'],['de','Axis']]:
        iCode = f'{{"{fac} rp "+((Integer.parseInt(LocationName.replaceAll("[^0-9]",""))+1+10) % 10)}}'
        dCode = f'{{"{fac} rp "+((Integer.parseInt(LocationName.replaceAll("[^0-9]",""))-1+10) % 10)}}'
        incr = SendtoTrait(mapName     = 'DeadMap',
                           name        = 'Increment',
                           restoreName = '',
                           restoreKey  = '',
                           destination = 'R', #R for region
                           region      = iCode,
                           key         = key('=',ALT_SHIFT))
        decr = SendtoTrait(mapName     = 'DeadMap',
                           name        = 'Decrement',
                           restoreName = '',
                           restoreKey  = '',
                           destination = 'R', #R for region
                           region      = dCode,
                           key         = key('-',ALT))
        rep = ReportTrait(key('=',ALT_SHIFT),key('-',ALT),
                          report = f'{{Faction+" RPs: "+BasicName.replaceAll("{fac} rp","")+'\
                              f'" x "+LocationName.replaceAll("[^0-9]","")}}')
        fac    = MarkTrait('Faction',faction)
        tpe    = MarkTrait('Type','RP')
        traits = [incr,decr,rep,fac,tpe,BasicTrait()]
        prototypeContainer.addPrototype(name        = f'{faction} RP',
                                        description = f'{faction} RP',
                                        traits      = traits)
        
    # ----------------------------------------------------------------
    # Add an inventory sorted on the unit supply and isolation status.
    disp = ('{PropertyValue==Faction ? Faction : '
            'Isolated_Level==1 ? "In-supply" : ' + \
            'Isolated_Level==2 ? "Isolated" : "Twice isolated"}')
    game.addInventory(include       = ('{(Faction=="Allied"||Faction=="Axis")&&'
                                       '(Type!="supply"&&Type!="RP")}'),
                      groupBy       = 'Faction,Isolated_Level',
                      sortFormat    = '$PieceName$',
                      tooltip       = 'Show supply status of pieces',
                      nonLeafFormat = disp, #'$PropertyValue$',
                      zoomOn        = True,
                      hotkey        = key('S',ALT),
                      refreshHotkey = key('S',ALT_SHIFT),
                      icon          = 'isolated-icon.png')
        

    # ----------------------------------------------------------------
    #
    # Remove 'Eliminate' trait from faction traits.  We will replace
    # that trait on each unit with a trait that moves the unit back to
    # its place on the OOB.
    #
    # We create the traits once, since they will be copied to the prototypes
    # Phases to allow the isolate command
    enableIsolate   = {'Allied':   [alliedSupply],
                       'Axis':     [axisSupply] }
    enableReset     = {'Allied':   [alliedCombat],
                       'Axis':     [axisCombat] }
    restrictIsolate = RestrictCommandsTrait(name='Restrict isolated command',
                                            hideOrDisable = 'Disable',
                                            expression = '',
                                            keys = [key('I')])
    restrictReset   = RestrictCommandsTrait(name='Restrict reset command',
                                            hideOrDisable = 'Disable',
                                            expression = '',
                                            keys       = [key('F')])

    isolated = LayerTrait(['',
                           'isolated-mark.png',
                           'twice-isolated-mark.png'],
                          ['','Isolated +','Twice isolated +'],
                          activateName = '',
                          activateMask = '',
                          activateChar = '',
                          increaseName = 'Isolated',
                          increaseMask = CTRL,
                          increaseChar = 'I',
                          decreaseName = '',
                          decreaseMask = '',
                          decreaseChar = '',
                          resetName    = 'Supplied',
                          resetKey     = key('F'),
                          under        = False,
                          underXoff    = 0,
                          underYoff    = 0,
                          loop         = False,
                          name         = 'Isolated',
                          description  = '(Un)Mark unit as isolated',
                          always       = False,
                          activateKey  = '',
                          increaseKey  = key('I'),
                          decreaseKey  = '',
                          scale        = 1)
    repIso  = ReportTrait(key('F'),key('I'),
                          report = '{{BasicName+" isolation level: "+Isolated_Level}}')
    actions = [elimKey,key('F')]
    trigger = TriggerTrait(name       = 'Eliminate unit',
                           command    = 'Eliminate',
                           key        = key('E'),
                           actionKeys = actions)
    toTobruk   = SendtoTrait(name        = 'To Tobruk',
                             mapName     = 'Board',
                             boardName   = 'Board',
                             key         = key(NONE,0)+',toTobruk',
                             restoreName = '',
                             restoreKey  = '',
                             destination = 'G',
                             position    = convHex('G25'))
    toBase   = SendtoTrait(name        = 'To Base',
                           mapName     = 'Board',
                           boardName   = 'Board',
                           key         = key(NONE,0)+',toBase',
                           restoreName = '',
                           restoreKey  = '',
                           destination = 'G',
                           position    = 'G25')
    repBase  = ReportTrait(key(NONE,0)+',toBase',key(NONE,0)+',toTobruk',
                           report = '{{BasicName+" send to "+LocationName}}')
    # Now modify the faction prototypes 
    for faction in ['Allied', 'Axis']:
        prototype = prototypes[f'{faction} prototype']
        traits    = prototype.getTraits()
        basic     = traits.pop()
        etrait    = Trait.findTrait(traits,SendtoTrait.ID,
                                    'name','Eliminate')
        if etrait is not None:
            etrait['name'] = ''
            etrait['key']=elimKey

        phases  = enableIsolate.get(faction,None)
        restrictIsolate['expression'] = ''
        if phases is not None:
            restrictIsolate['expression'] = \
                '{!(' + '||'.join(f'Phase=="{p}"' for p in phases) + ')}'

        phases = enableReset.get(faction,None)
        restrictReset['expression'] = ''
        if phases is not None:
            restrictReset['expression'] = \
                '{!(' + '||'.join(f'Phase=="{p}"' for p in phases) + ')'+\
                '&&CurrentBoard=="Board"}'
        
        toBase['position']   = convHex('J62' if faction == 'Allied' else 'W3')
        traits.extend([restrictReset,
                       restrictIsolate,
                       isolated,
                       repIso,
                       trigger,
                       toTobruk,
                       toBase,
                       repBase,
                       basic])            
        prototype.setTraits(*traits)

    # ----------------------------------------------------------------
    # Get all pieces and add Eliminate action that moves the unit to
    # the OOB. This is also where we add the traits to move the game
    # turn marker on the turn track.
    pieces  = game.getPieces(asdict=False)
    axes    = ['de ','it ']
    allies  = ['uk ','fr ','pl ','jw ','nz ','in ','sa ','au ']
    for piece in pieces:
        name    = piece['entryName'].strip()
        faction = name[:3]
        traits  = piece.getTraits()
        basic   = traits.pop()
        # Axis and Allied pieces
        if faction in axes+allies:
            factionName = 'Allied' if faction in allies else 'Axis'
            
            if 'supply' in name or ' rp ' in name:
                ftrait = Trait.findTrait(traits,PrototypeTrait.ID,
                                         'name',f'{factionName} prototype')
                if ftrait is not None:
                    traits.remove(ftrait)

                if ' rp ' in name:
                    traits.append(PrototypeTrait(factionName + ' RP'))
                        
                        
                # Eliminate trait - supply units are sent back to OOB,
                # not dead pool.
                if 'supply' in name:
                    factionMark = MarkTrait('Faction',factionName)
                    elim        = SendtoTrait(name        = 'Eliminate',
                                              mapName     = 'OOB',
                                              boardName   = 'OOB',
                                              key         = key('E'),
                                              restoreName = f'Restore {name}',
                                              restoreKey  = key('R'),
                                              destination = 'R',
                                              region      = name)
                    toBase['position'] = convHex('J62'
                                                 if factionName == 'Allied' 
                                                 else 'W3')
                    traits.extend([factionMark,elim,toTobruk,toBase])
                

            # Get the start-up hex from the upper right mark
            hx    = None
            start = Trait.findTrait(traits,MarkTrait.ID,
                                        'name', 'upper right')
            if start is not None:
                st = start['value']
                st = sub('[^=]+=','',st).strip()
                hx = convHex(st)
            
            # If we have a start up hex, create trait
            # print(f'{name} -> start = {hx}')
            if hx is not None:
                startKey = key(NONE,0)+',startHex'
                startup = SendtoTrait(name        = '',
                                      key         = startKey,
                                      mapName     = 'Board',
                                      boardName   = 'Board',
                                      destination = 'G',
                                      restoreName = '',
                                      restoreKey  = '',
                                      position    = hx)
                report = ReportTrait(startKey,
                                     report = '$newPieceName$ moved to start hex $location$')
                traits.extend([startup,report])


        # Add prototype traits to game turn marker.  Note, we
        # remove the basic piece trait from the end of the list,
        # and add it in after adding the other traits: The basic
        # piece trait _must_ be last.
        elif name == 'game turn':
            mtrait  = Trait.findTrait(traits,PrototypeTrait.ID,
                                       'name','Markers prototype')
            if mtrait is not None:
                traits.remove(mtrait)

            rtrait  = Trait.findTrait(traits,ReportTrait.ID)
            if rtrait is not None:
                traits.remove(rtrait)

            traits.extend([PrototypeTrait(pnn + ' prototype')
                           for pnn in turnPrototypes])

            traits.append(GlobalPropertyTrait(
                ['Is initial',isInit,GlobalPropertyTrait.DIRECT,
                 f'{{Turn!=1||Phase!="{axisSupply}"}}'
                 ],
                name = notSetup))

            traits.append(RestrictAccessTrait(sides=[],
                                              description='Cannot move'))
            # print('Modifying the game turn marker',traits)

        # Add basic trait as last trait and set traits
        traits.append(basic)
        piece.setTraits(*traits)

            
#
# EOF
#

                      
    
    
