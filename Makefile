#
#
#
NAME		:= AfrikaKorps
VERSION		:= 1.0
LATEX		:= lualatex
LATEX_FLAGS	:= -interaction=nonstopmode	\
		   -file-line-error		\
		   -synctex 15			\
		   -shell-escape
EXPORT          := $(shell kpsewhich wgexport.py)
EXPORT_FLAGS	:=
PDFTOCAIRO	:= pdftocairo
CAIROFLAGS	:= -scale-to 800
PDFJAM		:= pdfjam
REDIR		:= > /dev/null 2>&1 
DOCFILES	:= $(NAME).tex			\
		   front.tex			\
		   rules.tex			\
		   notes.tex
DATAFILES	:= materials.tex		\
		   hexes.tex			\
		   board.tex			\
		   tables.tex			\
		   oob.tex			\
		   wargame.rough.tex
STYFILES	:= ak.sty			\
		   commonwg.sty
BOARD		:= board.pdf
ROUGH		:= wargame.rough.pdf
SIGNATURE	:= 24
PAPER		:= --a4paper
TARGETS		:= $(NAME).pdf 			\
		   $(NAME)Booklet.pdf 		\
		   splitboard.pdf		\
		   materials.pdf		\
		   board.pdf

TARGETSA4	:= $(NAME)A4.pdf 		\
		   $(NAME)A4Booklet.pdf 	\
		   splitboardA3.pdf		\
		   splitboardA4.pdf		\
		   materialsA4.pdf		\
		   board.pdf			\
		   $(NAME).vmod

TARGETSLETTER	:= $(NAME)Letter.pdf 		\
		   $(NAME)LetterBooklet.pdf 	\
		   splitboardTabloid.pdf	\
		   splitboardLetter.pdf		\
		   materialsLetter.pdf

ifdef VERBOSE	
MUTE		:=
REDIR		:=
LATEX_FLAGS	:= -synctex 15	-shell-escape
EXPORT_FLAGS	:= -V
else
MUTE		:= @
REDIR		:= > /dev/null 2>&1 
endif

%.pdf:	%.tex
	@echo "LATEX	$*"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) $* $(REDIR)

%.aux:	%.tex
	@echo "LATEX(1)	$*"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) $* $(REDIR)

%.pdf:	%.aux
	@echo "LATEX(2)	$*"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) $* $(REDIR)

%A4.aux:%.tex
	@echo "LATEX(1)	$*A4"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*A4 $* $(REDIR)

%A4.pdf:%A4.aux
	@echo "LATEX(2)	$*A4"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*A4 $* $(REDIR)

%A4.pdf:%.tex
	@echo "LATEX	$* (A4)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*A4 $* $(REDIR)

%A3.aux:%.tex
	@echo "LATEX(1)	$*A3"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*A3 $* $(REDIR)

%A3.pdf:%A3.aux
	@echo "LATEX(2)	$*A3"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*A3 $* $(REDIR)

%A3.pdf:%.tex
	@echo "LATEX	$* (A3)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*A3 $* $(REDIR)


%Letter.aux:	%.tex
	@echo "LATEX(1)	$*Letter"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*Letter $* $(REDIR)

%Letter.pdf:	%Letter.aux
	@echo "LATEX(2)	$*Letter"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*Letter $* $(REDIR)

%Letter.pdf:	%.tex
	@echo "LATEX	$* (Letter)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*Letter $* $(REDIR)

%Tabloid.aux:	%.tex
	@echo "LATEX(1)	$*Tabloid"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*Tabloid $* $(REDIR)

%Tabloid.pdf:	%Tabloid.aux
	@echo "LATEX(2)	$*Tabloid"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*Tabloid $* $(REDIR)

%Tabloid.pdf:	%.tex
	@echo "LATEX	$* (Tabloid)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*Tabloid $* $(REDIR)


%Booklet.pdf:%.pdf
	@echo "BOOKLET	$*"
	$(MUTE)pdfjam 				\
		--landscape 			\
		--suffix book 			\
		--signature '$(SIGNATURE)' 	\
		$(PAPER) 			\
		--no-tidy 			\
		--outfile $@ 			\
		-- $< '1-$(SIGNATURE)' $(REDIR)

%.png:%.pdf
	$(PDFTOCAIRO) $(CAIROFLAGS) -singlefile -png $< $*

%.png:%A4.pdf
	$(PDFTOCAIRO) $(CAIROFLAGS) -singlefile -png $< $*

all:	$(TARGETS)
a4:	$(TARGETSA4)
us:	$(TARGETSLETTER)
vmod:	$(NAME).vmod
letter:	us
mine:	a4 spine.pdf logo.png frontA4.pdf 

cache:
	$(MUTE)mkdir -p cache
	$(MUTE)if test ! -f cache/.gitignore ; then touch cache/.gitignore; fi

distdir:$(TARGETS) README.md
	@echo "DISTDIR	$(NAME)-$(VERSION)"
	$(MUTE)mkdir -p $(NAME)-$(VERSION)
	$(MUTE)cp $^ $(NAME)-$(VERSION)/
	$(MUTE)touch $(NAME)-$(VERSION)/*

dist:	distdir
	@echo "DIST	$(NAME)-$(VERSION).zip"
	$(MUTE)zip -r $(NAME)-$(VERSION).zip $(NAME)-$(VERSION)
	$(MUTE)rm -rf $(NAME)-$(VERSION)

distdirA4:$(TARGETSA4) README.md
	@echo "DISTDIR	$(NAME)-A4-$(VERSION)"
	$(MUTE)mkdir -p $(NAME)-A4-$(VERSION)
	$(MUTE)cp $^ $(NAME)-A4-$(VERSION)/
	$(MUTE)touch $(NAME)-A4-$(VERSION)/*

distA4:	distdirA4
	@echo "DIST	$(NAME)-A4-$(VERSION).zip"
	$(MUTE)zip -r $(NAME)-A4-$(VERSION).zip $(NAME)-A4-$(VERSION)
	$(MUTE)rm -rf $(NAME)-A4-$(VERSION)

distdirLetter:$(TARGETSLETTER) README.md
	@echo "DISTDIR	$(NAME)-Letter-$(VERSION)"
	$(MUTE)mkdir -p $(NAME)-Letter-$(VERSION)
	$(MUTE)cp $^ $(NAME)-Letter-$(VERSION)/
	$(MUTE)touch $(NAME)-Letter-$(VERSION)/*

distLetter:	distdirLetter
	@echo "DIST	$(NAME)-Letter-$(VERSION).zip"
	$(MUTE)zip -r $(NAME)-Letter-$(VERSION).zip $(NAME)-Letter-$(VERSION)
	$(MUTE)rm -rf $(NAME)-Letter-$(VERSION)

oldschoolA4:
	@echo "Creating 'old-school' versions'"
	$(MUTE)$(MAKE) clean
	$(MUTE)touch .oldschool
	$(MUTE)$(MAKE) $(MAKEFLAGS) $(TARGETSA4) VERSION:=$(VERSION)-OS
	$(MUTE)mkdir -p oldschoolA4
	$(MUTE)mv $(TARGETSA4) oldschoolA4
	$(MUTE)$(MAKE) clean

oldschoolLetter:
	@echo "Creating 'old-school' versions'"
	$(MUTE)$(MAKE) clean
	$(MUTE)touch .oldschool
	$(MUTE)$(MAKE) $(MAKEFLAGS) $(TARGETSLETTER) VERSION:=$(VERSION)-OS
	$(MUTE)mkdir -p oldschoolLetter
	$(MUTE)mv $(TARGETSLETTER) oldschoolLetter
	$(MUTE)$(MAKE) clean 

clean:
	@echo "CLEAN"
	$(MUTE)rm -f *~ *.log *.aux *.out *.lot *.lof *.toc *.auxlock
	$(MUTE)rm -f *.synctex* *.pdf *-pdfjam.pdf *.vmod *.json
	$(MUTE)rm -f board.png board.svg logo.png tables.png
	$(MUTE)rm -f tables.png oob.png counters.png front.png 
	$(MUTE)rm -f $(TARGETS) $(TARGETSA4) $(TARGETSLETTER)
	$(MUTE)rm -f .oldschool
	$(MUTE)rm -f wargame.zip
	$(MUTE)$(MAKE) -C ak clean 

realclean: clean
	@echo "CLEAN ALL"
	$(MUTE)rm -rf cache/*  labels*.tex
	$(MUTE)rm -rf $(NAME)-*-$(VERSION)
	$(MUTE)rm -rf __pycache__ oldschool*
	$(MUTE)$(MAKE) -C ak clean 

distclean:realclean
	@echo "CLEAN DIST"
	$(MUTE)rm -rf *.zip
	$(MUTE)rm -rf $(NAME)-$(VERSION)
	$(MUTE)rm -rf $(NAME)-Letter-$(VERSION)
	$(MUTE)rm -rf $(NAME)-A4-$(VERSION)

$(NAME).pdf:		$(NAME).aux
$(NAME).aux:		$(DOCFILES) 	$(STYFILES) 	splitboardA4.pdf
materials.pdf:		$(DATAFILES)	$(STYFILES)	splitboardA4.pdf
splitboard.pdf:		splitboard.tex	$(STYFILES) 	$(BOARD)
board.pdf:		board.tex	hexes.tex	$(STYFILES) $(ROUGH)
$(ROUGH):		wargame.rough.tex

$(NAME)A4.pdf:		$(NAME)A4.aux
$(NAME)A4.aux:		$(DOCFILES) 	$(STYFILES)     splitboardA4.pdf
materialsA4.pdf:	$(DATAFILES)	$(STYFILES)	splitboardA4.pdf
splitboardA4.pdf:	splitboard.tex	$(STYFILES) 	$(BOARD)
splitboardA3.pdf:	splitboard.tex	$(STYFILES) 	$(BOARD)

$(NAME)Letter.pdf:	$(NAME)Letter.aux
$(NAME)Letter.aux:	$(DOCFILES) 	$(STYFILES) 	splitboardLetter.pdf
materialsLetter.pdf:	$(DATAFILES)	$(STYFILES)	splitboardLetter.pdf
splitboardLetter.pdf:	splitboard.tex	$(STYFILES) 	$(BOARD)
splitboardTabloid.pdf:	splitboard.tex	$(STYFILES) 	$(BOARD)

frontA4.pdf:		front.tex       board.pdf       $(STYFILES)
$(NAME)LetterBooklet.pdf:PAPER=--letterpaper

board-pdfjam.png:		CAIROFLAGS=-scale-to 1200

board-pdfjam.pdf:
	pdfjam --landscape --angle 270 --papersize '{37.6936cm,121.4374cm}' board.pdf

board.png:board-pdfjam.png
	rm board-pdfjam.pdf
	mv $< $@

export.pdf:	export.tex tables.tex hexes.tex oob.tex $(STYFILES)
rules.pdf:$(NAME)A4.pdf
	@echo "PDFJAM	$@ ($<)"
	$(MUTE)$(PDFJAM) \
		--outfile $@ 			\
		-- $< '1-$(SIGNATURE)' $(REDIR)

# Temporary patch.py rules.pdf
$(NAME).vmod:export.pdf patch.py rules.pdf
	@echo "VMOD	$(NAME).vmod"
	$(MUTE)$(EXPORT) export.pdf export.json -p patch.py \
		-v $(VERSION) -t "Afrika Korps" -r rules.pdf \
		-d "This module was created from LaTeX sources" \
		-o $(NAME).vmod -T Tutorial.vlog $(EXPORT_FLAGS) $(REDIR)

docker-prep:
	apt update
	apt install -y wget python3-pil poppler-utils 
	wget "https://gitlab.com/wargames_tex/wargame_tex/-/jobs/artifacts/master/download?job=dist" -O wargame.zip
	unzip -o wargame.zip -d ${HOME}
	mv ${HOME}/public ${HOME}/texmf
	make clean

docker-artifacts: distdirA4 distdirLetter oldschoolA4 oldschoolLetter

#
# EOF
#

