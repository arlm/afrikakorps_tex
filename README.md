# Afrika Korps

[[_TOC_]]

This is my remake of the wargame _Afrika Korps_.  The original game
was published by Avalon Hill Game Company in 1964.  This remake is
based on the third edition of the game from 1980. 

Also check out its sister game:
[D-Day](https://gitlab.com/wargames_tex/dday_tex/). 

## About this rework 

This rework is entirely new.  All text and graphics is new and nothing
is copied verbatim from the original materials. 

I have restructured and rewritten the rules somewhat (the text is all
new), as well as added illustrations of the rules.

## The board 

The board is quite big (37.5cm tall and 121.4cm wide), and does not
fit into readily available printers.  The board is therefore presented
as a 5-part document.  There are two version: 

- One designed to be printed on A3 paper ([splitboardA3.pdf][]).  This
  holds a relatively comfortable size of counters.  
  
- One designed to be printed on A4 paper ([splitboardA4.pdf][]).  This
  further split into to two giving 10 parts. 
  
(for US Tabloid and Letter, replace `A3` with `Tabloid` and `A4` with
`Letter`). 
  
Print your PDF of choice onto 5 or 10 separate pieces of paper (if you
have a duplex printer, simply print the document) and glue on to
sturdy cardboard (I like 1 1/2mm poster cardboard).  Cut along dashed
crop lines.  You can hold the 5 or 10 pieces together with paper
clamps or the like.

Alternatively you can glue the 5 or 10 sheets of paper together.  Cut
on end along the crop marks (dashed lines) and glue on to the next
part, taking care to align the crop mark arrows.  When playing the
game, put a transparent heavy plastic sheet on top to hold the board
down.

Remember to use the appropriate `materialsX.pdf` file for your board. 

| Board<br>Paper | Board<br>file | Materials<br>file | Materials<br>paper  |
|---------|---------------------------|--------------------------|--------|
| A3      | [splitboardA3.pdf][]      | [materialsA4.pdf][]      | A4     |
| A4      | [splitboardA4.pdf][]      | [materialsA4.pdf][]      | A4     |
| Tabloid | [splitboardTabloid.pdf][] | [materialsLetter.pdf][]  | Letter |
| Letter  | [splitboardLetter.pdf][]  | [materialsLetter.pdf][]  | Letter |

If an A3 print is not available, and the 10 part board is
unmanageable, then one can scale down [splitboardA3.pdf][] by a factor
of $`1/\sqrt{2}=0.7071`$ to get a document that can be printed on an
A4 printer.  In that case, one should _also_ scale down
[materialsA4.pdf][] by the same factor and use the counters and OOB
form that scaled down copy.

To scale down for Letter paper, one should apply a scale factor of
$`0.65`$ to both [splitboardTabloid.pdf][] and
[materialsLetter.pdf][]. 

## The files 

The distribution consists of the following files 

- [AfrikaKorpsA4.pdf][] This is a single document that contains
  everything: The rules, charts, counters, and order of battle, and
  the board split into 10 parts.
  
  This document is meant to be printed on a duplex A4 printer.  The
  charts, OOB charts, counters, and board will be on separate sheets
  and can be separated from the rules.
  
  Note that the PDF is a little heavy to load (due to the large map),
  so a bit of patience is needed. 
  
- [AfrikaKorpsA4Booklet.pdf][] This document _only_ contains the
  rules.  It is meant to be printed on a duplex A4 printer with long
  edge binding.  This will produce 6 sheets which should be folded
  down the middle of the long edge, and stabled to form an 24-page A5
  booklet of the rules.
  
- [splitboardA3.pdf][] holds the board in 5 sheets of A3 paper.  Print
  these and glue on to a sturdy piece of cardboard.  You can perhaps
  make some clever arrangement that allows you to unfold the map.
  Otherwise, use paper clambs or the like to hold the board pieces
  together.  
  
- [splitboardA4.pdf][] is like [splitboardA3.pdf][] above, but further
  split to fit on 10 sheets of A4 paper.   
  
- [materialsA4.pdf][] holds the charts, counters, and OOB.  It is
  meant to be printed on A4 paper.  Print and glue on to   a
  relatively thick piece of cardboard (1.5mm or so) or the like. 

- [board.pdf][] is the entire board in one PDF.  If you have a way of
  printing such a large sheet, perhaps this can be of use to you. 
  

If you only have access to US Letter (and possibly Tabloid) printer,
you can use the following files

| *A4 Series*                  | *Letter Series*                     |
| -----------------------------|-------------------------------------|
| [AfrikaKorpsA4.pdf][]        | [AfrikaKorpsLetter.pdf][]  	     |
| [AfrikaKorpsA4Booklet.pdf][] | [AfrikaKorpsLetterBooklet.pdf][]    |
| [materialsA4.pdf][]		   | [materialsLetter.pdf][]			 |
| [splitboardA4.pdf][]		   | [splitboardLetter.pdf][]			 |
| [splitboardA3.pdf][]		   | [splitboardTabloid.pdf][]			 |

Note, the US letter files are scaled down to 95% relative to the A4
files.  This is to fit everything in on the same scale.  This should
not be a problem for most users as most of the world is using A4
rather than the rather obscure Letter format. 

Download [artifacts.zip][] to get all files for both kinds of paper
formats. 

## "Old-school" version 

This version mimics the original graphics.  That is, it uses NATO
_friendly_ and _hostile_ colours for UK and Axis units,
respectively. Otherwise, it is the same as above. 

| *A4 Series*                    | *Letter Series*                       |
| -------------------------------|---------------------------------------|
| [AfrikaKorpsA4OS.pdf][]        | [AfrikaKorpsLetterOS.pdf][]  	     |
| [AfrikaKorpsA4BookletOS.pdf][] | [AfrikaKorpsLetterBookletOS.pdf][]    |
| [materialsA4OS.pdf][]		     | [materialsLetterOS.pdf][]			 |
| [splitboardA4OS.pdf][]		 | [splitboardLetterOS.pdf][]			 |
| [splitboardA3OS.pdf][]		 | [splitboardTabloidOS.pdf][]			 |

## VASSAL Module 

There is a [VASSAL](https://vassalengine.org) module available 

- [AfrikaKorps.vmod][] 
- [AfrikaKorpsOS.vmod][] (old-school version)

This is made from the same sources as the Print'n'Play documents. 

## Previews 

[![The board](.imgs/board.png)](https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-A4-master/board.pdf?job=dist "The board")
[![The rules](.imgs/front.png)](https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-A4-master/AfrikaKorpsA4.pdf?job=dist)
[![The charts](.imgs/tables.png)](https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-A4-master/materialsA4.pdf?job=dist)
[![The OOB](.imgs/oob.png)](https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-A4-master/materialsA4.pdf?job=dist)
[![The counters](.imgs/counters.png)](https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-A4-master/materialsA4.pdf?job=dist)


## Implementation 

The whole package is implemented in LaTeX using my package
[_wargame_](https://gitlab.com/wargames_tex/wargame_tex).  This package,
combined with the power of LaTeX, produces high-quality documents,
with vector graphics to ensure that everything scales.   Since the
board is quite big, we must use Lua$`\mathrm{\LaTeX}`$ for the
formatting. 

## The General articles 

[All _The General_
issues](https://www.vftt.co.uk/ah_mags.asp?ProdID=PDF_Gen) as scanned PDFs.

- [Volume 1, issue 4](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2010%20No%204.pdf)
- Replay:
  [Volume 13, issue 5](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2013%20No%205.pdf)
- Rules rationals: [Volume 18, issue 1](https://www.vftt.co.uk/files/AH%20The%20General/The%20General%20Vol%2018%20No%201.pdf)


[artifacts.zip]: https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/download?job=dist

[AfrikaKorpsA4.pdf]: https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-A4-master/AfrikaKorpsA4.pdf?job=dist
[AfrikaKorpsA4Booklet.pdf]: https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-A4-master/AfrikaKorpsA4Booklet.pdf?job=dist
[materialsA4.pdf]: https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-A4-master/materialsA4.pdf?job=dist
[splitboardA3.pdf]: https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-A4-master/splitboardA3.pdf?job=dist
[splitboardA4.pdf]: https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-A4-master/splitboardA4.pdf?job=dist
[board.pdf]: https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-A4-master/board.pdf?job=dist
[AfrikaKorps.vmod]: https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-A4-master/AfrikaKorps.vmod?job=dist


[AfrikaKorpsLetter.pdf]: https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-Letter-master/AfrikaKorpsLetter.pdf?job=dist
[AfrikaKorpsLetterBooklet.pdf]: https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-Letter-master/AfrikaKorpsLetterBooklet.pdf?job=dist
[materialsLetter.pdf]: https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-Letter-master/materialsLetter.pdf?job=dist
[splitboardTabloid.pdf]: https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-Letter-master/splitboardTabloid.pdf?job=dist
[splitboardLetter.pdf]: https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/AfrikaKorps-Letter-master/splitboardLetter.pdf?job=dist




[AfrikaKorpsA4OS.pdf]: https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/oldschoolA4/AfrikaKorpsA4.pdf?job=dist
[AfrikaKorpsA4BookletOS.pdf]: https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/oldschoolA4/AfrikaKorpsA4Booklet.pdf?job=dist
[materialsA4OS.pdf]: https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/oldschoolA4/materialsA4.pdf?job=dist
[splitboardA3OS.pdf]: https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/oldschoolA4/splitboardA3.pdf?job=dist
[splitboardA4OS.pdf]: https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/oldschoolA4/splitboardA4.pdf?job=dist
[boardOS.pdf]: https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/oldschoolA4/board.pdf?job=dist
[AfrikaKorpsOS.vmod]: https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/oldschoolA4/AfrikaKorps.vmod?job=dist


[AfrikaKorpsLetterOS.pdf]: https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/oldschoolLetter/AfrikaKorpsLetter.pdf?job=dist
[AfrikaKorpsLetterBookletOS.pdf]: https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/oldschoolLetter/AfrikaKorpsLetterBooklet.pdf?job=dist
[materialsLetterOS.pdf]: https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/oldschoolLetter/materialsLetter.pdf?job=dist
[splitboardTabloidOS.pdf]: https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/oldschoolLetter/splitboardTabloid.pdf?job=dist
[splitboardLetterOS.pdf]: https://gitlab.com/wargames_tex/afrikakorps_tex/-/jobs/artifacts/master/file/oldschoolLetter/splitboardLetter.pdf?job=dist





